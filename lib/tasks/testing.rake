namespace :testing do
  
  desc "Validate distance of the recorded position to door2door office"
  task distance: :environment do
    comment "=> Starting..."
    puts :environment
    d2d = [52.53, 13.403]
    positions = Position.all
    puts Position.count
    positions.each do |p|
      distance = Geocoder::Calculations.distance_between(d2d, [p.lat, p.lng])
      puts "#{distance>3.5 ? 'KO!!' : '>>>>'} distance:#{distance} - id:#{p.id} - lat:#{p.lat}, lng:#{p.lng}"
    end

    comment "=> Finished!"
  rescue StandardError => e
    comment "=> ERROR!!!!!: #{e.message}"
  end




  #######################################
  def comment(msg)
    puts "#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    puts "#   #{msg}"
    puts "#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  end
end
