class CreateCarriers < ActiveRecord::Migration[6.1]
  def change
    create_table :carriers do |t|
      t.string :uuid, null: false

      t.timestamps
    end
  end
end
