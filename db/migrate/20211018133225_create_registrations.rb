class CreateRegistrations < ActiveRecord::Migration[6.1]
  def change
    create_table :registrations do |t|
      t.datetime :checkin, null: false
      t.datetime :checkout
      t.references :carrier, null: false, foreign_key: true

      t.timestamps
    end

  end
end
