class CreatePositions < ActiveRecord::Migration[6.1]
  def change
    create_table :positions do |t|
      t.float :lat, null: false
      t.float :lng, null: false
      t.datetime :at, null: false
      t.float :bearing
      t.references :registration, null: false, foreign_key: true

      t.timestamps
    end

  end
end
