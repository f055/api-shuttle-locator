class AllygatorChannel < ApplicationCable::Channel
  def subscribed
    logger.info "@@@@@@@@@@@@@@@@@  subscribed"
    stream_from "allygator_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
  
end
