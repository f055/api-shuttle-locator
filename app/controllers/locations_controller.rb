class LocationsController < ApplicationController

  # POST /vehicles/:ID/locations
  def create

    carrier = Carrier.find_by(uuid: params["id"])
    if !carrier.blank?
        registration = Registration.where(carrier: carrier, checkout: ['', nil]).first
        @position = Position.new(lat: params["lat"], lng: params["lng"], at: params["at"], registration: registration)

        if @position.save
            render json: {}, status: 204

            ActionCable.server.broadcast("allygator_channel", { lat: @position.lat, lng: @position.lng, at: @position.at, vehicle: carrier.uuid})
        else
            render json: @position.errors, status: :unprocessable_entity
        end 
        
    else
        render json: {"error": "Vehicle without registration or not found"}, status: 422
    end

  end

end
