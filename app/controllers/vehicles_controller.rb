class VehiclesController < ApplicationController
    
  # POST /vehicles
  def create
    @registration = Registration.new
    @registration.create_session(params[:id])
    if @registration.errors.blank?
        render json: {}, status: 204
        ActionCable.server.broadcast("allygator_channel", { vehicle: params[:id], action: "registration"})
    else
        render json: @registration.errors, status: :unprocessable_entity
    end 
  end

  # DELETE /vehicles/1
  def destroy 
   
    carrier = Carrier.where(uuid: params[:id]).first
    
    @registration = Registration.where(carrier: carrier, checkout: [nil, ""]).first
    if @registration.destroy_session
        render json: {}, status: 204
        ActionCable.server.broadcast("allygator_channel", { vehicle: params[:id], action: "de-registration"})
    else
        render json: @registration.errors, status: :unprocessable_entity
    end 
    rescue StandardError => e
      render json: {"error":"carrier or registration unexistant"}, status: :unprocessable_entity
  end

end
