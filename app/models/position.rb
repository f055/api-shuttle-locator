class Position < ApplicationRecord
  belongs_to :registration
  validates :lat, :lng, :at, presence: true
  validates :lat, :lng, numericality: true
  validates :bearing, numericality: true, allow_nil: true
  before_save :set_bearing
  validate :validate_boundary

  #d2d => 52.53, 13.403

  def calculate_distance
    pos = [self.lat, self.lng]
    d2d = [52.53,13.403]
    Geocoder::Calculations.distance_between(d2d, pos)
  rescue StandardError => e
    nil
  end

  def validate_boundary
    max_distance = 3.5
    if calculate_distance > max_distance
      errors.add(:lat, "Vehicle is out of boundary.")
    end
  rescue StandardError => e
    errors.add(:base, e.message)
  end

  def calculate_bearing
    last = Position.where(registration_id: self.registration_id).last
    from = [last.lat, last.lng]
    pos = [self.lat, self.lng]
    Geocoder::Calculations.bearing_between(from,pos)
  rescue StandardError => e
    nil  
  end

  def set_bearing
    self.bearing = self.calculate_bearing
  end
  
end
