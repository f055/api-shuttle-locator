class Registration < ApplicationRecord
  belongs_to :carrier
  has_many :positions
  validates :checkin, :carrier_id, presence: true

  def create_session(uuid)
    ActiveRecord::Base.transaction do
        #uuid = params[:id]
        carrier = Carrier.find_or_create_by(uuid: uuid)
        self.carrier = carrier
        self.checkin = Time.now()
        self.save!
    end
  rescue StandardError => e
      nil
  end

  def destroy_session
    ActiveRecord::Base.transaction do
      self.checkout = Time.now()
      self.save!
    end
    true
  rescue StandardError => e
    false
  end

end

