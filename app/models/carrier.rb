class Carrier < ApplicationRecord
    has_many :registrations
    validates :uuid, presence: true
    validates :uuid, uniqueness: { case_sensitive: false }


end
