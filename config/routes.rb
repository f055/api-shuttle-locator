Rails.application.routes.draw do
  get 'health/index'

  post 'vehicles' => 'vehicles#create'
  delete 'vehicles/:id' => 'vehicles#destroy'
  post 'vehicles/:id/locations' => 'locations#create'

  mount ActionCable.server => '/cable'
end
