
server 'localhost', roles: [:web, :app, :db], primary: true

set :branch,          "development"
set :stage,           :development
set :deploy_to,       "/users/#{fetch(:user)}/docker/#{fetch(:application)}"
set :keep_releases,   3
set :final_path,      "#{fetch(:deploy_to)}/#{fetch(:application)}"
set :app_path,        "/Users/foss/Dropbox/dev/ruby/d2d/api-shuttle-locator"

#append :linked_files, '.env'

namespace :deploy do
  desc "Copy docker files to final path"
  task :copy_docker_compose do
    on roles(:app) do
      puts ">>>>> Copy docker-compose files to final path"
      begin
        execute "rm -R #{fetch(:final_path)}"
      rescue
        execute "echo rm failed"
      end
      execute "cp -LR #{fetch(:app_path)} #{fetch(:final_path)}"
    end
  end

  desc 'Run Docker-compose'
  task :start do
    puts ">>>>> Start application @ Docker-Compose"
    on roles(:app) do
      execute "cd #{fetch(:app_path)} && docker-compose up"
    end
  end

  before :starting,  :check_revision
  before :starting, 'deploy:set_version'
  before :starting, 'deploy:build_docker_image'
  #before :starting, 'deploy:push_docker_image'
  after :publishing, :copy_docker_compose
  after :publishing, 'deploy:cleanup'
  #after :publishing, 'deploy:start'
end
