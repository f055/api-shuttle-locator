require 'tempfile'
require 'fileutils'

# config valid for current version and patch releases of Capistrano
lock "~> 3.16.0"

set :application,   "api-shuttle-locator"
set :user,          "foss"
set :repo_url,      "git@bitbucket.org:f055/api-shuttle-locator.git"
set :pty,           true
set :use_sudo,      false
set :deploy_via,    :copy #:remote_cache
set :app_version,   ["0.1.0"]

namespace :deploy do
    desc "Make sure local git is in sync with remote."
    task :check_revision do
      on roles(:app) do
        puts ">>>>> Revision check"
        unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
          puts "WARNING: HEAD is not the same as origin/#{fetch(:branch)}"
          puts "Run `git push` to sync changes."
          exit
        end
      end
    end
  
    desc "Retrieve app version and revision"
    task :set_version do
      run_locally do
        puts ">>>>> Retrive application Version and Revision from GIT"
        revision = `SHA1=$(git rev-parse --short HEAD 2> /dev/null); if [ $SHA1 ]; then echo $SHA1; else echo 'unknown'; fi`.chomp
        version = `VERSION=$(git describe --tags 2> /dev/null); if [ $VERSION ]; then echo $VERSION; else echo 'unknown'; fi`.chomp
        version = version.gsub(";","")
        revision = revision.gsub(";","")
        append :app_version, version
        begin
          execute "cd #{fetch(:app_path)} && rm git_version.yml"
          puts ">>>>> Set_version: v#{version} :: rev#{revision}"
          execute "echo 'version: #{version}' > #{fetch(:app_path)}/git_version.yml"
          execute "echo 'revision: #{revision}' >> #{fetch(:app_path)}/git_version.yml"
  
          # save retrieved version to .env file
          puts ">>>>> Set_version: Start writing version to .env file"
          path = fetch(:app_path) + "/.env"
          temp_file = Tempfile.new('temp_file')
          begin
            File.open(path, 'r') do |file|
              file.each_line do |line|
                temp_file.puts line.gsub(/APP_VERSION_TAG=\s*(.*)/,"APP_VERSION_TAG=#{version}")
              end
            end
            temp_file.close
            FileUtils.mv(temp_file.path, path)
          ensure
            temp_file.close
            temp_file.unlink
            puts ">>>>> Set_version: Finished writing version to .env file"
          end
  
        rescue
          execute "cd #{fetch(:app_path)} && echo -e 'ERROR retrieving app version date=#{Time.now.utc}'"
        end
      end
    end
  
    desc 'Build Rails Docker Image'
    task :build_docker_image do
      run_locally do
        puts ">>>>> Build rails docker image"
        execute "docker build --platform linux/amd64 -t marcelofossrj/d2d:#{fetch(:app_version).last} ."
      end
    end
  
    desc 'Push Docker Image to Doker-Hub'
    task :push_docker_image do
      run_locally do
        puts ">>>>> PUSH docker image to dockerhub"
        execute "docker login && docker push marcelofossrj/d2d:#{fetch(:app_version).last}"
      end
    end
  end


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
