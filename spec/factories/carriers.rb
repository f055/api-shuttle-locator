
FactoryBot.define do
  factory :carrier do
    sequence(:uuid) { |n| "abc#{n}" }
  end
end