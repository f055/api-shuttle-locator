FactoryBot.define do
  factory :position do
    registration
    sequence(:lat) { |n| (52.530934 - n/200) }
    sequence(:lng) { |n| (13.409910 - n/1000) }
    sequence(:at) { |n| (Time.now + (3*n).minutes) }

    factory :position_outbound do
      sequence(:lat) { |n| (52.550934 - n/200) }
      sequence(:lng) { |n| (13.239910 - n/1000) }
    end
    factory :position_from do
      lat { 52.520934 }
      lng { 13.379910 }
    end
    factory :position_to do
      lat { 52.515718 }
      lng { 13.377449 }
    end
  end
end
#52.520934, 13.379910
#52.515718, 13.377449
#52.511186, 13.376888
#52.505032, 13.381602

#d2d => 52.53, 13.403

