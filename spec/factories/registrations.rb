FactoryBot.define do
  factory :registration do
    carrier 
    checkin { Time.now }
    checkout { nil }
  end
end
