require 'rails_helper'

RSpec.describe Position, type: :model do

  describe 'associations' do
    it { should belong_to(:registration) }
  end
  describe 'validations' do
    it { should validate_presence_of(:lat) }
    it { should validate_presence_of(:lng) }
    it { should validate_presence_of(:at) }
    it { should validate_numericality_of(:lat)}
    it { should validate_numericality_of(:lng)}
    it { should validate_numericality_of(:bearing)}
    #it { should validate_uniqueness_of(:at).scoped_to(:registration_id)}

    scenario 'validate bearing' do
      # register the vehicle
      registration = FactoryBot.create(:registration) 
      # generate a position
      from = FactoryBot.build(:position_from)
      position = FactoryBot.build(:position_to)
      from1 = Position.create(lat: from.lat, lng: from.lng, at: from.at, registration_id: registration.id)
      position1 = Position.create(lat: position.lat, lng: position.lng, at: position.at, registration_id: registration.id)
      expect(position1.bearing).to eq(196.01998957713283 )
    end

    it 'validates that the carrier is outbound' do
      # register the vehicle
      registration = FactoryBot.create(:registration) 
      # generate a position
      position = FactoryBot.build(:position_outbound)
      expect(position).to_not be_valid
    end

    it 'validates that the carrier is inbound' do
      # register the vehicle
      registration = FactoryBot.create(:registration) 
      # generate a position
      position = FactoryBot.build(:position)
      expect(position).to be_valid
    end
  end

end
