require 'rails_helper'

RSpec.describe Carrier, type: :model do
  describe 'associations' do
    it { should have_many(:registrations) }
  end
  describe 'validations' do
    it { should validate_presence_of(:uuid) }
    it { is_expected.not_to allow_value('').for(:uuid) }

    subject { Carrier.new(uuid: "abc123") }
    it { should validate_uniqueness_of(:uuid).ignoring_case_sensitivity }
  end
end



