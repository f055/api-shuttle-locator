require 'rails_helper'

RSpec.describe Registration, type: :model do
  describe 'associations' do
    it { should have_many(:positions) }
    it { should belong_to(:carrier) }
  end
  describe 'validations' do
    it { should validate_presence_of(:checkin) }
    it { should validate_presence_of(:carrier_id) }
    #it { should validate_uniqueness_of(:checkin).scoped_to(:carrier_id)}
  end
end
