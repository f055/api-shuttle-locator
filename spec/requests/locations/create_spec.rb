require 'rails_helper'

RSpec.describe "Locations", type: :request do
  describe "POST /create" do
    before(:each) do
      # register the vehicle
      registration = FactoryBot.create(:registration)
      @carrier = registration.carrier  
      # generate a position
      @position = FactoryBot.build(:position)
      @action_cable = ActionCable.server
    end

    scenario 'valid attributes' do
      # send a POST request to /vehicles
      post "/vehicles/#{@carrier.uuid}/locations", params: { lat: @position.lat, lng: @position.lng, at: @position.at} 
  
      # response should have HTTP Status 204
      expect(response.status).to eq(204)
      
      # check the value of the returned json
      json = response.body
      @expected = ''
      expect(json).to eq(@expected)
  
      # 1 new registration record is created
      expect(Position.count).to eq(1)
    end

    scenario 'invalid attributes: missing position' do
      # send a POST request to /vehicles
      post "/vehicles/#{@carrier.uuid}/locations", params: {at: @position.at} 
  
      # response should have HTTP Status 422
      expect(response.status).to eq(422)
      
      # check the value of the returned json
      json = JSON.parse(response.body)
      expect(json["lat"]).to eq(["can't be blank", "is not a number" ])
  
      # 1 new registration record is created
      expect(Position.count).to eq(0)
    end


    scenario 'invalid attributes: missing missing time' do
      # send a POST request to /vehicles
      post "/vehicles/#{@carrier.uuid}/locations", params: { lat: @position.lat, lng: @position.lng} 
  
      # response should have HTTP Status 422
      expect(response.status).to eq(422)
      
      # check the value of the returned json
      json = JSON.parse(response.body)
      expect(json["at"]).to eq(["can't be blank"])
  
      # 1 new registration record is created
      expect(Position.count).to eq(0)
    end

    scenario 'invalid attributes: non registered vehicle' do
      # send a POST request to /vehicles
      post "/vehicles/non234/locations", params: { lat: @position.lat, lng: @position.lng , at: @position.at} 
  
      # response should have HTTP Status 422
      expect(response.status).to eq(422)
      
      # check the value of the returned json
      json = JSON.parse(response.body)
      #@expected = {"error": "Vehicle without registration or not found"}
      expect(json["error"]).to eq("Vehicle without registration or not found")
  
      # 1 new registration record is created
      expect(Position.count).to eq(0)
    end

  end
end
