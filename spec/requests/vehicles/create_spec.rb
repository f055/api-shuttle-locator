require 'rails_helper'

RSpec.describe "Vehicles", type: :request do
  describe "POST /create" do
    
    scenario 'valid attributes' do
      # send a POST request to /vehicles
      post '/vehicles', params: {id: 'abc123'} 
  
      # response should have HTTP Status 204
      expect(response.status).to eq(204)
      
      # check the value of the returned json
      json = response.body
      @expected = ''
      expect(json).to eq(@expected)
  
      # 1 new registration record is created
      expect(Registration.count).to eq(1)
      expect(Carrier.count).to eq(1)
  
      # Optionally, you can check the latest record data
      expect(Carrier.last.uuid).to eq('abc123')
    end


    scenario 'invalid attributes' do
      post '/vehicles', params: {ID: ''} 
  
      expect(response.status).to eq(422)
  
      json = JSON.parse(response.body).deep_symbolize_keys
      expect(json[:carrier_id]).to eq(["can't be blank"])
  
      # no new bookmaCarrierrk record is created
      expect(Carrier.count).to eq(0)
      expect(Registration.count).to eq(0)
    end



  end
end
