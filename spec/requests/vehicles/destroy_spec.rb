require 'rails_helper'

RSpec.describe "Vehicles", type: :request do
  describe "DELETE /destroy" do

    scenario 'valid attributes' do
      registration = FactoryBot.create(:registration)

      # send a POST request to /vehicles
      delete "/vehicles/#{registration.carrier.uuid}" 
  
      # response should have HTTP Status 204
      expect(response.status).to eq(204)
      
      # check the value of the returned json
      json = response.body
      @expected = ''
      expect(json).to eq(@expected)
  
      # 1 new registration record is created
      expect(Registration.count).to eq(1)
      expect(Carrier.count).to eq(1)
  
      expect(Registration.last.checkout).not_to eq(nil)
    end

    scenario 'not valid attributes' do
      registration = FactoryBot.create(:registration)

      # send a POST request to /vehicles
      delete "/vehicles/98989"

      # response should have HTTP Status 204
      expect(response.status).to eq(422)

      expect(Registration.last.checkout).to eq(nil)
    end

    scenario 'carrier or registration unexistant' do


      # send a POST request to /vehicles
      delete "/vehicles/98989"

      # response should have HTTP Status 204
      expect(response.status).to eq(422)

      json = JSON.parse(response.body).deep_symbolize_keys
      expect(json[:error]).to eq("carrier or registration unexistant")
    end


  end
end
