require 'rails_helper'

RSpec.describe AllygatorChannel, type: :channel do

  scenario "subscribes to the service" do
    subscribe()

    expect(subscription).to be_confirmed
  end

end
