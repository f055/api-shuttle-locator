FROM ruby:2.7.0-slim

RUN apt-get update \
    && apt-get install -y curl gpg wget gnupg2

RUN curl https://deb.nodesource.com/setup_12.x | bash
RUN curl https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get install -y nodejs yarn
RUN apt-get install -y build-essential libpq-dev vim

# Copy application code
COPY . /app
# Change to the application's directory
WORKDIR /app

# Install gems
RUN bundle install

ENV RAILS_ENV production

RUN chmod +x entrypoint.sh

ENTRYPOINT ./entrypoint.sh






