# Api-shuttle-locator

The api-shuttle-locator is a project create to address the code challenge from door2door - Berlin, where:
*door2door, the operator of the 'allygator shuttle' service in Berlin, collects the live position of all vehicles in its fleet in real-time via a GPS sensor in each vehicle. To manage the service, door2door needs you to provide a solution which can:
- collect the location and compute the navigation bearing (direction of vehicle expressed in angles) of all vehicles in real-time
- broadcast the location & bearing updates of all vehicles to connected clients, like web or mobile applications.*

The full challenge description can be found at: <https://github.com/door2door-io/d2d-code-challenges/tree/main/backend>

## Quick start
The project is composed of three main pieces:
1. The API server, which is the main task of the challenge;
2. A client application that receives the broadcasted data from the API server and show the shuttles in a map;
3. The simulation tool provided by doo2door

### Running the project

#### 1. Setup API server
The API server is provided as a docker-compose multi-container runtime. The project is located at the _api-shuttle-locator directory_, and the corresponding docker-compose configuration files are located in its root. run the commands below to have it running:
```
$ docker-compose create
$ docker-compose up
```
Ther server will be available at `http://127.0.0.1:3000`
to ensure everything is alright, run the command below in another terminal session:
````
$ curl http://127.0.0.1:3000/health/index
````
and you should receive the response:
````
$ {"status":"online"}
````

#### 2. Run the client
The client application is a simple html/javscript project, and is located at the _client_ directory. Simply open the `allygator_shuttle,html` file on a browser (preferablly Chrome or Safari).

#### 3. Run the driver simulator tool
Follow the instructions at <https://github.com/door2door-io/d2d-code-challenges/tree/main/resources/driver-simulator> to have it running.

Once the the driver simulator tool starts posting requests, you should be able to see the shuttles moving in _real time_ at the browser.

## End points
The api interfaces were implemented following the challenge specification.

## Approach and design considerations
The API is implemented using Ruby and Ruby on Rails. It is a RoR 6.1.4 project running in api only mode, using ActionCable (Websockets), to broadcast the shuttles positions to the client app. 
The database used to store the shuttle locations is PostgreSql, for caching it is used Redis. The selection of that platform comes from two perspectives:
- It is a stable, well known and easy to maintain codebase and platform, and more the important, can handle the job well.
- It is the platform I am more comfortable with.

### The database
The choice of PostgreSql over MongoDB, or relational over document database, 
it was a tought one, and I am not totally sure if I took the better decision. 
Watching purely performace for fast writes and no strong need for transactions, 
MongoDb would be the better choice. But, in the other hand, the reasoning for 
storing the data is for analysis, hence having more structured data is a bit better, 
so with the information avalable I made the choice for PostgreSql.  
The model is described in the `model.pdf` file in the root folder of the zip file.  
I went throught a normalized model approach for the same reason described above, 
and if there is the need to de-normalize it for perfomence, it is not a complex change.
I did not use much validation constraints. I assumed that if one vehicle is posting 
his position, its because he is available to work, so lets track his location, 
and make him available, but this is something to be validated with product owner, and understand 
better how the devices operate and other technical constraints.

### API
CORS, Rack-Attack are good options for general security and JWT for authentication. 
JWT was not implemented for two reasons: to make it easy to test, it was not explicit in the specification.
To calculate the bearing and the radius from door2door office I used _Geocoder_ gem. 
I've been using it for a while, it does a good job for geocoding, it is well implemented
and it is the most forked and watched one in Github.
Tests were created with RSpec which is my preferred library for that task, and I would say that I
implemented the minimum acceptable amount of tests.
I used Postman, curl and the _driver simulation tool_ to test API requests.

### Client application
It is a plain html/javascript/JQuery application. I used with the main purpose to test the 
broadcasting out of RSpec and as visualization. I wanted something lightweight that could 
be constructed fast, with no need of api key, and fit to the main purpose. With that goal I 
went for Leaflet with OpenStreetMap for map interface library. It was quite easy to 
implement and has a good number of resources, although it needs a benchmark test before using it 
in production. It was the first time I used, so far so good.  
Regarding the map markers, it is missing the implementation of the vehicle de-registration.

## Benchmarking / Scaling up
I was not able to go further in benchmarkig/stress testing. I configured the production 
environment to use 5 workers and up to 8 threads, in a docker machine running with 4CPUs and 3GBRAM,
in an Mac M1 (not yet fully native). I used the wrk tool <https://github.com/wg/wrk> to run 
the tests. I disabled active_storage and action_mail which made a huge improvement 
in the latency. 
The results were:
````
$ wrk -t5 -c1000 -d30 -s ~/Dropbox/dev/ruby/d2d/post_req.lua http://127.0.0.1:3000/vehicles
Running 30s test @ http://127.0.0.1:3000/vehicles
  5 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   207.54ms  452.02ms   1.98s    90.37%
    Req/Sec   167.75     83.67   454.00     65.48%
  24736 requests in 30.10s, 8.37MB read
  Socket errors: connect 753, read 257, write 47, timeout 63
Requests/sec:    821.72
Transfer/sec:    284.77KB
````
There different options to improve these results, from increasing RAM and CPU to load balance.

## Next Steps
- Implement versioning  
  My normal approach for versioning is to use URL segment,
  which is using a version slug string in the resource identifier, e.g. /v1/vehicles/abc123.

- Implement HTTPS and authentication with JWT

- Improve the tests

## Final thoughts

In overall it was a great experience, that I really enjoyed.
I focused in covering most of the features, including the extras, 
so I could have the better overview of the solution, and detect further improvements.

Marcelo Foss
